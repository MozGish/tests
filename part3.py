def info(func):
    """Этот декоратор выведет в консоль название функции, которую оборачивает"""
    def wrapper(*args, **kwargs):
        print(f'Function ‘{func.__name__}’ was called')
        val = func(*args, **kwargs)
        return val
    return wrapper


def decor_with_parametr(argument):
    """Декоратор с параметром, который умножает послежний аргумент на число"""
    def the_real_decorator(function):
        def wrapper(*args, **kwargs):
            new_argunet = [*args][:-1]
            new_argunet.append([*args][-1]*argument)
            result = function(*new_argunet, **kwargs)
            return result
        return wrapper
    return the_real_decorator


@decor_with_parametr(3) 
def summ(*args, reverse=False):
    if reverse ==True:
        args = [*args]
        args.reverse()
    return sum(args)

@info
def minus(*args, reverse=False):
    if reverse ==True:
        args = [*args]
        args.reverse()
    elem = args[0]
    for i in args:
        elem=elem-i
    return elem

def division(*args, reverse=False):
    if reverse ==True:
        args = [*args]
        args.reverse()
    elem = args[0]
    for i in args:
        elem=elem/i
    return elem

def multiplication(*args, reverse=False):
    if reverse ==True:
        args = [*args]
        args.reverse()
    elem = args[0]
    for i in args:
        elem=elem*i
    return elem



if __name__ == "__main__":
    print(summ(12,2,3,4, reverse=True))
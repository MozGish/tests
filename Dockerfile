FROM python:3.9-alpine

RUN python -m pip install --upgrade pip

COPY . /flask_new

WORKDIR /flask_new

RUN pip install -r requirements.txt

EXPOSE 5005
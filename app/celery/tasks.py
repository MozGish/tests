from app.celery import celery


@celery.task
def send_mail():
    pass


@celery.task
def send_async_email(msg):
    """Фоновое задание по отправки email с помощью
Flask-Mail."""
    with app.app_context():
        mail.send(msg)
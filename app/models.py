from app import db


class Room(db.Model):
    __tablename__ = 'room'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(64),unique=True)
    racks = db.relationship('Rack', backref='room')

    def __repr__(self) -> str:
        return f'<Room id:{str(self.id)} name:{self.name}'
    
    def get_dict(self):
        return {'id':self.id, 'name':self.name}


class Client(db.Model):
    __tablename__ = 'client'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(64),unique=True)
    racks = db.relationship('Rack', backref='customer')

    def __repr__(self) -> str:
        return f'<Client id:{str(self.id)} name:{self.name}'
    
    def get_dict(self):
        return {'id':self.id, 'name':self.name}

class Rack(db.Model):
    __tablename__ = 'rack'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(64),unique=True)
    size = db.Column(db.Integer)
    state = db.Column(db.String(64))
    customer_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))

    def __repr__(self) -> str:
        return f'<Rack id:{str(self.id)} name:{self.name}'
    
    def get_dict(self):
        return {'id':self.id, 'name':self.name, 'size':self.size,'state':self.state, 
            'customer_id':self.customer_id, 'room_id':self.room_id}


tables = {'rooms':Room,'racks':Rack, 'clients':Client}
tables_key = tables.keys()
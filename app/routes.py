# -*- coding: utf-8 -*-
from app import app
from flask import render_template, make_response
from app.controllers import *
from flask import request, url_for


# Описание заданий
@app.route('/')
@app.route('/index')
def index():
    request = {'name':'Тестовое задание', 'body':'Таран Юрий Петрович: taran.iu@selectel.ru'}
    return render_template('part2.html', request=request)


# Главная страница
@app.route('/description')
def check_description():
    des = descriptions()
    return render_template('description.html', description=des)


# from requests import request
@app.route('/article/', methods=['POST',  'GET'])
def article():
    if request.method == 'POST':
        print(request.form)
        res = make_response("")
        res.set_cookie("font", request.form.get('font'), 60*60*24*15)
        res.headers['location'] = url_for('article')
        return res, 302
    print("html")
    return render_template('email.html')

#Для теста проводим гет и post запрос
@app.route('/post/', methods=['POST',  'GET'])
def article():
    if request.method == 'POST':
        print(request.form)
        res = make_response("")
        res.set_cookie("font", request.form.get('font'), 60*60*24*15)
        res.headers['location'] = url_for('article')
        return res, 302
    print("html")
    return render_template('email.html')


# Наполнение базы данных
@app.route('/data-fill')
def fill():
    text = fill_data_base()
    return render_template('part1.html', text=text)


# Первое задание
@app.route('/part-1')
def distributor_part():
    return render_template('part1.html', text='Hello world!')


@app.route('/part-3')
def decorators():
    request = {'name':'Задание 3', 'body':'Декораторы можно найти в корне в part3.py.'}
    return render_template('part2.html', request=request)


# Сортировки 2 уровень

# Получение всех строк всех таблиц
@app.route('/part-2/<type>')
def get_all(type):
    request=get_all_elements(type)
    return render_template('part2.html', request=request)


@app.route('/part-2/get_occupied_rack')
def get_sort():
    request = {'name':'Выборка занятых стоек', 'body':get_occupied_rack()}
    return render_template('part2.html', request=request)


@app.route('/part-2/size_room')
def get_sort_size_room():
    request = {'name':'Узнать стойку с максимальной высотой для каждой комнаты', 'body':get_max_size_in_room()}
    return render_template('part2.html', request=request)


@app.route('/part-2/room_and_clients')
def get_sort_room():
    request = {'name':'Выборка клиентов по комнате', 'body':get_room_and_clinet_with_occupied()}
    return render_template('part2.html', request=request)


# Описание для 2 задания что вводить
@app.route('/part-2')
@app.route('/part-2/')
def get_part2():
    request = {'name':'Задание 2', 'body':'Можете вызвать сортировки /racks, /clients, /rooms, /get_occupied_rack, /size_room. /room_and_clients'}
    return render_template('part2.html', request=request)


# Получение всех строк всех таблиц
@app.route('/<type>')
def get_404(type):
    request = {'name':"🌚 404 🌝",'body':"Такой страницы не найдено"}
    return render_template('part2.html', request=request)




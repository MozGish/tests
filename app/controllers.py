from urllib import response
from app import db
from app.data import clients, racks, rooms, description
from app.models import Rack,Room,Client, tables, tables_key



def clear_db():
    try:
        db.session.query(Rack).delete()
        db.session.query(Room).delete()
        db.session.query(Client).delete()
        db.session.commit()
    except:
        db.session.rollback()
        db.session.commit()

# # Функция заполняющая базу
def fill_data_base():
    """Простая и банальная функция заполнения, это функция для тестирования и проверки больше"""
    client_obj = []
    rack_obj = []
    room_obj = []
    
    clear_db()

    for room in rooms:
        room_obj.append(Room(name=room['name']))

    for client in clients:
        client_obj.append(Client(name=client['name']))

    for rack in racks:
        rack_obj.append(Rack(name=rack['name'],size=rack['size'],state=rack['state'],
        customer=client_obj[int(rack['customer'])-1],
        room=room_obj[int(rack['room'])-1]))

    obj = client_obj+room_obj
    db.session.add_all(obj)
    db.session.commit()

    db.session.add_all(rack_obj)
    db.session.commit()
    return "Всё заново записалось  👌"


#Вывод на страницу описания
def descriptions():
    return description


# Функция отдачи всех строк
def get_all_elements(type:str)->str:
    if type in tables_key:
        data = tables[str(type)].query.all()
        dict_data =[]
        for elements in data:
            dict_data.append(elements.get_dict())
        return {'name':f'All in table {type.upper()}','body':dict_data}
    else:
        return {'name':'🤷', 'body':'Такой таблицы нет'}


# Получить занятые стойки и параметры 
def get_occupied_rack():
    """Поля в результирующей таблице: id стойки, name стойки, name клиента, name комнаты"""
    response = []
    racks = Rack.query.filter_by(state='occupied').all()
    for rack in racks:
        response.append(
            {'id':rack.id, 'rack_name':rack.name, 'customer_name':rack.customer.name, 'room_name':rack.room.name}
        )
    return response

"""
    3. Выборка клиентов по комнате
   
    Получить список всех комнат с прикреплённым к каждой массивом из ID тех клиентов, у которых есть занятые 
    (status == ‘occupied’) стойки в этой комнате.
    Поля в результирующей таблице: id комнаты, name комнаты, массив id клиентов
"""
def get_room_and_clinet_with_occupied():
    rooms = Room.query.all()
    room_and_client = []
    for room in rooms:
        client=[]
        for rack in room.racks:
            if rack.state=='occupied':
                client.append(rack.customer)
        room_and_client.append({
            'room_id':room.id,
            'room_name':room.name,
            'client': set(client) 
        })
    #Надо тестить, при зависимостях таблиц при join они ведут себя странно выяснил к сожалению это уже слишком поздно
    # с = Rack.query.filter_by(state='occupied').join(Client, Client.id==Rack.customer).all()
    return room_and_client

"""
    4. Узнать стойку с максимальной высотой для каждой комнаты

    Нужно получить список всех комнат и для каждой определить стойку внутри неё, 
    у которой поле size имеет наибольшее значение. Если таких стоек несколько, можно выбрать любую.
    Поля в результирующей таблице: id комнаты, id стойки, size стойки
"""
def get_max_size_in_room():
    rooms = Room.query.all()
    win = []
    for room in rooms:
        max_size=0
        rack_id =0
        for rack in room.racks:
            if rack.size>max_size:
                max_size = rack.size
                rack_id = rack.id
        win.append({'room_id':room.id,'rack_id':rack_id,'max':max_size})
    return win


from app import app, db
from app.models import Client, Rack, Room

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Client':Client ,'Rack': Rack, 'Room': Room}

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5005, use_reloader=True)
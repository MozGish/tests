import pytest

@pytest.mark.parametrize("test_input,expected", [([3,5], 8), ([2,2], 6), ([6,9], 15)])
def test_lol(test_input, expected):
    print(test_input)
    new = test_input[0] + test_input[1]
    assert new == expected, f'test_lol {test_input[0]} + {test_input[1]} != {expected}'
